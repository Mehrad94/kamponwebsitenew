import owner from "./owner";
import discount from "./discount";
import club from "./club";
import slider from "./slider";
import banner from "./banner";

const Delete = { owner, discount, club, slider, banner };
export default Delete;

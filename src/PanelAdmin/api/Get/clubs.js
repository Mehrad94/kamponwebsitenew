import axios from "../axios-orders";
import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const clubs = async (returnData, loading) => {
  return axios
    .get(Strings.ApiString.CLUB)
    .then(clubs => {
      console.log({ clubs });
      returnData(clubs.data);
      loading(false);
    })
    .catch(error => {
      console.log({ error });
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
    });
};

export default clubs;

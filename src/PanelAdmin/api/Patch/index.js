import category from "./category";
import editDiscount from "./discount";
import club from "./club";
import owner from "./owner";
import slider from "./slider";
import banner from "./banner";
const Patch = {
  category,
  editDiscount,
  club,
  owner,
  slider,
  banner
};
export default Patch;

import axios from "../axios-orders";
import axios2 from "axios";

import Strings from "../../value/PanelString";
import toastify from "../../util/toastify";

const imageUpload = async (files, setLoading, type, setState, cancelUpload) => {
  setLoading(true);
  // ============================================= const
  const CancelToken = axios2.CancelToken;
  const source = CancelToken.source();
  const settings = {
    onUploadProgress: progressEvent => {
      let percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);

      setState(prev => ({ ...prev, progressPercentImage: percentCompleted }));
    },
    cancelToken: source.token
  };
  const URL = Strings.ApiString.UPLOAD;
  const formData = new FormData();
  formData.append("imageName", files.name.split(".")[0]);
  formData.append("imageType", type);
  formData.append("image", files);

  //=============================================== axios
  return axios
    .post(URL, formData, settings)
    .then(Response => {
      console.log({ Response });
      setLoading(false);
      return Response.data;
    })
    .catch(error => {
      console.log({ error });
      setLoading(false);
      if (error.message === "Network Error") toastify("دسترسی به اینترنت را بررسی کنید", "error");
      else toastify("خطایی در سرور رخ داده. لطفا دوباره تلاش کنید", "error");
      return "";
    });
};
export default imageUpload;

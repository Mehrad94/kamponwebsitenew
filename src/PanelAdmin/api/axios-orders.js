import axios from "axios";
import Cookie from "js-cookie";

const instance = axios.create({ baseURL: "https://kampon.ir/api/v1" });
instance.defaults.headers.common["Authorization"] = "Bearer " + Cookie.get("TOKEN");

export default instance;

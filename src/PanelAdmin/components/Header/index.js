import React from "react";
import "./index.scss";
import HeaderProfile from "./HeaderProfile";

const Header = () => {
	return (
		<nav className="panelAdmin-navbar-container">
			<div className="panel-navbar-box">
				<ul className="panel-navbar-notifications">
					<li className="pointer hoverColorblack normalTransition">
						<i className="icon-search"></i>
					</li>
					<li className="navbar-icon-massege pointer hoverColorblack normalTransition">
						<div className="massege-icon">
							<i className="icon-mail "></i>
							<span className="show-modal-icon-value">4</span>
						</div>
					</li>
				</ul>
				<HeaderProfile />
			</div>
		</nav>
	);
};

export default Header;

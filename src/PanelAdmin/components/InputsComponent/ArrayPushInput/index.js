import React, { useState, useRef } from "react";
import "./index.scss";
const ArrayPushInput = props => {
  const { accepted, name, label, inputType } = props;
  const [Typing, setTyping] = useState("");
  const acceptedClick = () => {
    if (Typing.length > 2) {
      accepted(Typing);
      setTyping("");
    }
  };
  const submitRef = useRef(null);

  const handelOnkeyDown = e => {
    if (e.key === "Enter") {
      submitRef.current.click();
    }
  };
  return (
    <div className="input-push-wrapper">
      <input
        type={inputType}
        className="transition0-3"
        onKeyDown={handelOnkeyDown}
        name={name}
        placeholder={label}
        value={Typing}
        onChange={e => setTyping(e.currentTarget.value)}
        autoComplete="off"
      />
      <span ref={submitRef} type="submited" onClick={acceptedClick}>
        ثبت
      </span>
    </div>
  );
};

export default ArrayPushInput;

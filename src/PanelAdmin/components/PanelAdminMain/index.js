import React from "react";
import "./index.scss";
import { Switch, Route, Redirect } from "react-router-dom";
import pageRoutes from "../../value/pageRoutes";
import ProtectedRoute from "../../util/ProtectedRoute";
import formatRoute from "../../util/formatRoute";
import LoginScreen from "../../screen/LoginScreen";

const PanelAdminMain = () => {
  return (
    <main className="panelAdmin-main-container">
      <Switch>
        {formatRoute.map((route, index) => {
          return <ProtectedRoute key={index} path={route.path} component={route.component} />;
        })}
        <Redirect from={pageRoutes.GS_ADMIN_LOGIN} to={pageRoutes.GS_ADMIN_DASHBOARD} />

        <Redirect exact from={pageRoutes.GS_PANEL_ADMIN_TITLE} to={pageRoutes.GS_ADMIN_DASHBOARD} />
      </Switch>
    </main>
  );
};

export default PanelAdminMain;

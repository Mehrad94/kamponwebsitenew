import React from "react";
import "./index.scss";
import CardElement from "../CardElement";

const ShowCardInformation = ({ data, onClick, submitedTitle, optionClick, options }) => {
  const showDataAll = data.map((information, index) => {
    // //console.log({ information });
    return <CardElement key={index + "m"} data={information} options={options} optionClick={optionClick} index={index} onClick={onClick} submitedTitle={submitedTitle} />;
  });
  const ShowData = <CardElement data={data} onClick={onClick} options={options} optionClick={optionClick} submitedTitle={submitedTitle} />;

  return <div className="show-card-elements">{data.length > 0 ? showDataAll : ShowData}</div>;
};

export default ShowCardInformation;

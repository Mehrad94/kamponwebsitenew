import React from "react";
import { Link } from "react-router-dom";
import logo from "../../assets/Images/img/logo-white.png";
import MainMenu from "../../components/SideMenu/MainMenu";
import menuData from "../../util/menuFormat";
import "./index.scss";

const SideMenu = () => {
  return (
    <div className="panelAdmin-sideBar-container ">
      <Link to={"/panelAdmin"} className={"lgHolder"}>
        {/* <img src={logo} alt={"logo"} /> */}
        دکتر کلابز
      </Link>
      <MainMenu mainMenus={menuData} />
    </div>
  );
};

export default SideMenu;

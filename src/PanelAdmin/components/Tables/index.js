import React from "react";
import { Table } from "react-bootstrap";
import "./index.scss";
const TableBasic = props => {
  const { thead, tbody, onClick } = props;

  const optionElement = (option, index) => (
    <td key={index + "m"} className="centerAll table-wrapper ">
      {option.edit ? (
        <span>
          <i className="icon-edit-alt" onClick={() => onClick(index, "edit")} />
        </span>
      ) : (
        ""
      )}
      {option.remove ? (
        <span>
          <i className="icon-cancel-circled" onClick={() => onClick(index, "remov")} />
        </span>
      ) : (
        ""
      )}
    </td>
  );
  return (
    <Table striped bordered hover size="sm" className="table-wrapper">
      <thead>
        <tr>
          {thead.map((infoHead, parentIndex) => {
            return (
              <th className="textCenter" key={parentIndex}>
                {infoHead}
              </th>
            );
          })}
        </tr>
      </thead>
      <tbody>
        {tbody.map((infoBody, parentIndex) => {
          return (
            <tr key={parentIndex}>
              <td className="textCenter">{parentIndex + 1}</td>
              {infoBody.map((info, childIndex) => {
                const pattern = RegExp("http");
                return info ? (
                  pattern.test(info) ? (
                    <td key={childIndex} className="textCenter">
                      <img style={{ width: "25px" }} src={info} alt="image" />
                    </td>
                  ) : info.option ? (
                    optionElement(info.option, parentIndex)
                  ) : (
                    <td key={childIndex} className="textCenter">
                      {info}
                    </td>
                  )
                ) : (
                  ""
                );
              })}
            </tr>
          );
        })}
      </tbody>
    </Table>
  );
};

export default TableBasic;

import React from "react";

import "./index.scss";
import InputPush from "./InputPush";
import InputFile from "./InputFile";
import SearchDropDown from "./SearchDropDown";
import InputDropDownSearch from "./InputDropDownSearch";
import InputFileArray from "./InputFileArray";

const Inputs = props => {
  const {
    progress,
    elementType,
    elementConfig,
    value,
    changed,
    accepted,
    label,
    invalid,
    shouldValidate,
    touched,
    removeHandel,
    dropDownData,
    defaultInputDesable,
    checkSubmited,
    disabled,
    display,
    searchAccepted,
    setSearchAccepted,
    staticTitle
  } = props;
  let inputElement = null;
  const inputClasses = ["InputElement"];

  if (invalid && shouldValidate && touched) {
    inputClasses.push("Invalid");
  }

  switch (elementType) {
    case "input":
      inputElement = <input disabled={disabled} className={inputClasses.join(" ")} {...elementConfig} value={value} onChange={changed} />;
      break;
    case "inputPush":
      inputElement = <InputPush disabled={disabled} className={inputClasses.join(" ")} accepted={accepted} value={value} elementConfig={elementConfig} removeHandel={removeHandel} />;
      break;

    case "inputFile":
      inputElement = <InputFile disabled={disabled} progress={progress} className={inputClasses.join(" ")} onChange={changed} value={value} {...elementConfig} inputLabel={"انتخاب"} label={label} />;
      break;
    case "InputFileArray":
      inputElement = (
        <InputFileArray removeHandel={removeHandel} disabled={disabled} progress={progress} className={inputClasses.join(" ")} onChange={changed} value={value} {...elementConfig} inputLabel={"انتخاب"} label={label} />
      );
      break;

    case "inputSearch":
      inputElement = (
        <SearchDropDown
          checkSubmited={checkSubmited}
          dropDownData={dropDownData}
          accepted={accepted}
          className={inputClasses.join(" ")}
          onChange={changed}
          value={value}
          label={label}
          elementConfig={elementConfig}
          disabled={disabled}
          searchAccepted={searchAccepted}
          setSearchAccepted={setSearchAccepted}
          staticTitle={staticTitle}
        />
      );
      break;
    case "inputDropDownSearch":
      inputElement = (
        <InputDropDownSearch dropDownData={dropDownData} disabled={disabled} className={inputClasses.join(" ")} accepted={accepted} value={value} elementConfig={elementConfig} checkSubmited={checkSubmited} />
      );
      break;
    case "textarea":
      inputElement = <textarea disabled={disabled} className={inputClasses.join(" ")} {...elementConfig} value={value} onChange={changed} />;
      break;
    case "select":
      inputElement = (
        <select disabled={disabled} className={inputClasses.join(" ")} value={value} onChange={changed}>
          {elementConfig.options.map(option => (
            <option key={option.value} value={option.value}>
              {option.displayValue}
            </option>
          ))}
        </select>
      );
      break;
    default:
      return defaultInputDesable ? "" : (inputElement = <input className={inputClasses.join(" ")} {...elementConfig} value={value} onChange={changed} />);
  }

  return (
    <div className={"Input"} style={{ display }}>
      <label className={"Label"}>{label}</label>
      {inputElement}
    </div>
  );
};

export default Inputs;

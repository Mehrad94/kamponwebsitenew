import React from "react";
import "./index.scss";

const InputFile = props => {
  const { onChange, inputLabel, name, value, progress, disabled, cancelUpload, placeholder } = props;

  const elements = (
    <React.Fragment>
      <div>{value ? value : "  انتخاب نمایید ..."}</div>
      <label>
        <span>{progress ? progress + "%" : inputLabel}</span>
        <input disabled={progress ? true : disabled} type="file" onChange={onChange} name={name} />
      </label>
    </React.Fragment>
  );
  return <div className="addFileModalContainer">{elements}</div>;
};

export default InputFile;

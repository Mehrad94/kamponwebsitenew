import React, { Fragment, useState } from "react";
import "./index.scss";

const InputFileArray = props => {
  const { onChange, inputLabel, name, value, progress, disabled, cancelUpload, placeholder, removeHandel } = props;
  const [state, setState] = useState({ Form: {}, formIsValid: false });

  const elements = (
    <Fragment>
      {value.length > 0 && (
        <div className="data-show-array">
          {value.map((data, index) => {
            return (
              <span onClick={() => removeHandel(data)} key={index}>
                {data}
              </span>
            );
          })}
        </div>
      )}
      <div className="addFileModalContainer">
        <div>{"  انتخاب نمایید ..."}</div>
        <label>
          <span>{progress ? progress + "%" : inputLabel}</span>
          <input disabled={progress ? true : disabled} type="file" onChange={onChange} name={name} />
        </label>
      </div>
    </Fragment>
  );
  return elements;
};

export default InputFileArray;

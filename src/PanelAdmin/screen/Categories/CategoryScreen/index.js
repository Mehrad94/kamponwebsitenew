import React, { useEffect, useState } from "react";
import "./index.scss";
import { get, post } from "../../../api";
import { BoxLoading } from "react-loadingg";
import ModalInput from "../../../util/modals/modalInput";
import { Table } from "react-bootstrap";
import TabelBasic from "../../../components/Tables";
import ShowCategory from "../../../util/consts/table/ShowCategory";
import ModalBox from "../../../util/modals/ModalBox";
import AddCategoryInputs from "../../../util/input/operationInput/AddCategoryInputs";
import inputArray from "../../../util/input/inputArray";
import AddCategory from "../AddCategory";

const CategoryScreen = () => {
  const [Categories, setCategories] = useState([]);
  const [Loading, setLoading] = useState(true);
  const [editData, setEditData] = useState(false);
  const [InitialState, setInitialState] = useState(false);
  const [Modal, setModal] = useState({
    show: false
  });

  useEffect(() => {
    handelgetApi();
  }, []);
  const handelgetApi = () => {
    setEditData(false);
    get.categoreis(setCategories, setLoading);
  };
  // ======================================== modal

  const onHideModal = () => {
    setModal({ ...Modal, show: false });
    setEditData(false);
    setInitialState(!InitialState);
  };
  const onShowlModal = files => {
    setModal({ ...Modal, show: true });
  };
  // ======================================== End modal =========================
  const _tableOnclick = (index, work) => {
    if (work === "edit") {
      onShowlModal();
      setEditData(Categories[index]);
    }
  };
  const showDataElement = (
    <div className="countainer-main centerAll ">
      <div className="elemnts-box  boxShadow">
        <div className="subtitle-elements">
          <span className="centerAll">دسته بندی ها</span>
          <div className="btns-container">
            <button onClick={onShowlModal} className="btns btns-primary">
              افزودن +
            </button>
          </div>
        </div>

        <div className="show-elements">
          <TabelBasic tbody={ShowCategory(Categories)[1]} thead={ShowCategory(Categories)[0]} onClick={_tableOnclick} />
        </div>
      </div>
    </div>
  );
  return Loading ? (
    <BoxLoading size={"large"} />
  ) : (
    <React.Fragment>
      <ModalBox onHideModal={onHideModal} showModal={Modal.show}>
        {" "}
        <AddCategory InitialState={InitialState} onHideModal={onHideModal} handelgetApi={handelgetApi} editData={editData} />
      </ModalBox>

      {showDataElement}
    </React.Fragment>
  );
};

export default CategoryScreen;

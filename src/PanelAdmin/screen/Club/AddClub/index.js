import React, { useRef, useEffect, useState, Fragment } from "react";
import "./index.scss";
import { post } from "../../../api";
import ModalBox from "../../../util/modals/ModalBox";
import ModalTrueFalse from "../../../util/modals/ModalTrueFalse";
import FormInputClub from "./FormInputClub";
import onChanges from "../../../util/onChanges";
import updateObject from "../../../util/updateObject";
import states from "../../../util/consts/states";
const AddClub = () => {
  const [data, setData] = useState(states.addClub);
  const [state, setState] = useState({ progressPercentImage: null, remove: { value: "", name: "" } });
  const [removeStateData, setRemoveStateData] = useState("");
  const [Loading, setLoading] = useState(false);
  const [Modal, setModal] = useState({
    show: false
  });
  const [checkSubmited, setCheckSubmited] = useState(false);
  // ======================================== modal
  const onHideModal = () => setModal({ ...Modal, show: false });

  const onShowlModal = () => setModal({ ...Modal, show: true });

  // ========================= End modal =================
  // ============================= submited
  const submited = () => setCheckSubmited(!checkSubmited);
  const _onSubmited = async e => {
    e.preventDefault();
    const formData = {};
    for (let formElementIdentifier in data.Form) if (formElementIdentifier !== removeStateData) formData[formElementIdentifier] = data.Form[formElementIdentifier].value;
    const initialStateMembership = updateObject(states.addClub.Form["membership"], { value: [] });
    const initialStateSlides = updateObject(states.addClub.Form["slides"], { value: [] });
    const updatedForm = updateObject(states.addClub.Form, { ["membership"]: initialStateMembership, ["slides"]: initialStateSlides });

    if (await post.club(formData)) {
      submited();
      setData({ Form: updatedForm, formIsValid: false });
    }
  };
  // ========================= End submited =================
  // ============================= remove
  const __returnPrevstep = value => {
    onHideModal();
    setState({ ...state, remove: { value: "", name: "" } });
    if (value) inputChangedHandler({ name: state.remove.name, value: state.remove.value });
  };
  const removeHandel = (value, name) => {
    onShowlModal();
    setState({ ...state, remove: { value, name } });
  };
  // =========================== End remove  ====================
  const inputChangedHandler = async (event, initial) => {
    let datas = initial ? states.addClub : data;
    let validName;
    if (initial) {
      if (event.value === "CLUB") validName = "discount";
      else validName = "club";
      setRemoveStateData(validName);
      submited();
    }
    await onChanges.globalChange({ event, data: datas, setData, state, setState, setLoading, imageType: "sliders", validName });
  };
  const stateArray = [];
  for (let key in data.Form) stateArray.push({ id: key, config: data.Form[key] });
  let form = (
    <FormInputClub
      removeHandel={removeHandel}
      _onSubmited={_onSubmited}
      stateArray={stateArray}
      state={state}
      setData={setData}
      Loading={Loading}
      setLoading={setLoading}
      inputChangedHandler={inputChangedHandler}
      checkSubmited={checkSubmited}
    />
  );
  return (
    <div className="countainer-main centerAll ">
      <ModalBox onHideModal={onHideModal} showModal={Modal.show}>
        <ModalTrueFalse modalHeadline={"آیا مطمئن به حذف آن هستید !"} modalAcceptTitle={"بله"} modalCanselTitle={"خیر"} modalAccept={__returnPrevstep} />
      </ModalBox>
      <div className="form-countainer">
        <div className="form-subtitle">افزودن باشگاه جدید</div>
        <div className="row-give-information">
          {form}
          <div className="btns-container">
            <button className="btns btns-primary" disabled={!data.formIsValid} onClick={_onSubmited}>
              افزودن{" "}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddClub;

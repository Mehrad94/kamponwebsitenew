import React, { useState, useEffect, useMemo } from "react";
import "./index.scss";
import { Button, ButtonToolbar } from "react-bootstrap";
import { post, put, patch, get } from "../../../../api";
import DetailsCheckBox from "../../../../components/DetailsComponent/DetailsCheckBox";
import DetailsStringTextArea from "../../../../components/DetailsComponent/DetailsStringTextArea";
import DetailsStringInput from "../../../../components/DetailsComponent/DetailsStringInput";
import DetailsArrayCheckBox from "../../../../components/DetailsComponent/DetailsArrayCheckBox";
import DetailsArrayTextArea from "../../../../components/DetailsComponent/DetailsArrayTextArea";
import DetailsArrayThumbnail from "../../../../components/DetailsComponent/DetailsArrayThumbnail";
import toastify from "../../../../util/toastify";
import DetailsInputSearch from "../../../../components/DetailsComponent/DetailsInputSearch";
const ClubDetails = ({ information, handelback, refreshComponent, sendNewValData }) => {
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(true);

  let CatSearch = [];
  let CatInfo = { value: information.category._id, title: information.category.titleFa };

  useEffect(() => {
    get.categoreis(setCategories, setLoading);
  }, []);
  for (const index in categories) CatSearch.push({ value: categories[index]._id, title: categories[index].titleFa });

  // ================================================= modalInputs ======================
  const sendNewVal = async data => {
    console.log({ data });
    let param;
    param = Object.assign({ data }, { _id: information._id });
    sendNewValData(param);
  };
  // =================================================End modalInputs ======================
  console.log({ information });
  const elements = (
    <React.Fragment>
      <div className="show-card-elements-details opacity-Fade-in-adn-slide-top ">
        <ButtonToolbar>
          <Button onClick={handelback} variant="outline-primary">
            بازگشت <span>{">>>"}</span>
          </Button>
        </ButtonToolbar>
        <DetailsArrayThumbnail toastify={toastify} sendNewVal={sendNewVal} label={"اسلایدس"} elementType={"inputFile"} imageType={"thumbnail"} fieldName={"slides"} cover={information.slides} />

        <div className="detailsRow-wrapper">
          <div>
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.title} Id={information._id} fieldName={"title"} label={"عنوان"} />
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.owner.title} Id={information._id} fieldName={"title"} label={"فروشنده"} fixed={true} />
            <DetailsStringTextArea toastify={toastify} sendNewVal={sendNewVal} Info={information.aboutClub} Id={information._id} fieldName={"aboutClub"} label={"درباره باشگاه"} />
            <DetailsCheckBox toastify={toastify} sendNewVal={sendNewVal} Info={information.isActive} Id={information._id} fieldName={"isActive"} questions={{ question1: "فعال", question2: "غیر فعال" }} label={"وضعیت"} />
          </div>
          <div>
            <DetailsArrayCheckBox toastify={toastify} sendNewVal={sendNewVal} Info={information.membership} Id={information._id} fieldName={"membership"} questions={["APPLICATION", "CARD"]} label={"عضویت"} />
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.percent} Id={information._id} fieldName={"percent"} label={"درصد"} />
            <DetailsArrayTextArea toastify={toastify} sendNewVal={sendNewVal} Info={information.comments} fieldName={"comments"} label={"  نظرات "} />
            <DetailsInputSearch toastify={toastify} sendNewVal={sendNewVal} Info={CatInfo} searchInfo={CatSearch} fieldName={"category"} label={"دسته بندی  "} />
          </div>
        </div>
      </div>
    </React.Fragment>
  );

  return elements;
};

export default ClubDetails;

import React, { useEffect, useState } from "react";

import { get, patch, deletes } from "../../../api";
import ShowCardInformation from "../../../components/ShowCardInformation";
import BoxLoading from "react-loadingg/lib/BoxLoading";
import card from "../../../util/consts/card";
import ClubDetails from "./ClubDetails";
const ClubScreen = () => {
  const [Clubs, setClubs] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadingUploadImage, setLoadingUploadImage] = useState(false);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [state, setstate] = useState();
  console.log(Clubs);
  let index = 0;

  useEffect(() => {
    _handelShowSections(acceptedIndex);
  }, [state]);

  useEffect(() => {
    apiClubs();
  }, []);

  const apiClubs = async () => {
    return await get.clubs(setClubs, setLoading);
  };

  const _handelShowSections = index => {
    setAcceptedIndex(index);
  };
  const _handelback = () => {
    setAcceptedIndex();
    apiClubs();
  };
  const refreshComponent = async () => {
    if (await apiClubs()) {
      setstate(index++);
    }
  };
  const sendNewValData = async param => {
    let Info = await patch.club(param);
    if (Info) refreshComponent();
  };

  const optionClick = async event => {
    switch (event.mission) {
      case "remove":
        if (await deletes.club(event._id)) refreshComponent();
        break;
      case "block":
        let data = { fieldChange: "isActive", newValue: event.value.toString() };
        let param = Object.assign({ data }, { _id: event._id });
        sendNewValData(param);
        break;
      default:
        break;
    }
  };
  const showDataElement =
    Clubs.length > 0 ? (
      acceptedIndex >= 0 ? (
        <React.Fragment>
          <ClubDetails information={Clubs[acceptedIndex]} handelback={_handelback} refreshComponent={refreshComponent} sendNewValData={sendNewValData} />
        </React.Fragment>
      ) : (
        <ShowCardInformation data={card.club(Clubs)} options={{ remove: true, block: true }} onClick={_handelShowSections} optionClick={optionClick} submitedTitle={"مشاهده جزيیات"} />
      )
    ) : (
      ""
    );
  return (
    <React.Fragment>
      <div className="countainer-main">{loading ? <BoxLoading size={"large"} /> : <React.Fragment>{showDataElement}</React.Fragment>}</div>;
    </React.Fragment>
  );
};

export default ClubScreen;

import React, { useState } from "react";
import Inputs from "../../../../components/UI/Inputs/Input";
import "./index.scss";
import { get } from "../../../../api";
const FormInputDiscount = props => {
  const { stateArray, removeHandel, state, _onSubmited, inputChangedHandler, checkSubmited } = props;
  const [searchCat, setSearchCat] = useState([]);
  const [searchOwner, setSearchOwner] = useState([]);

  let searchCatData = [];
  let searchOwnerData = [];
  for (const index in searchCat) searchCatData.push({ value: searchCat[index]._id, title: searchCat[index].titleFa, description: searchCat[index].titleEn, image: searchCat[index].image });

  for (const index in searchOwner) searchOwnerData.push({ value: searchOwner[index]._id, title: searchOwner[index].title, description: searchOwner[index].subTitle, image: searchOwner[index].thumbnail });

  const searchedCat = e => {
    setSearchOwner([]);
    if (e.currentTarget.value.length >= 2) get.categoreisSearch(e.currentTarget.value, setSearchCat);
  };
  const searchedOwner = e => {
    setSearchCat([]);
    if (e.currentTarget.value.length >= 2) get.ownersSearch(e.currentTarget.value, setSearchOwner);
  };
  const accept = event => {
    setSearchOwner([]);
    setSearchCat([]);
    inputChangedHandler(event);
  };
  return (
    <form onSubmit={_onSubmited}>
      {stateArray.map(formElement => {
        const invalid = !formElement.config.valid;
        const shouldValidate = formElement.config.validation;
        const touched = formElement.config.touched;
        let changed, accepted, dropDownData;

        const inputClasses = ["InputElement"];
        if (invalid && shouldValidate && touched) inputClasses.push("Invalid");

        if (formElement.id === "category") {
          changed = searchedCat;
          accepted = value => accept({ value, name: formElement.id });
          dropDownData = searchCatData;
        } else if (formElement.id === "owner") {
          changed = searchedOwner;
          accepted = value => accept({ value, name: formElement.id });
          dropDownData = searchOwnerData;
        } else {
          changed = e => inputChangedHandler({ value: e.currentTarget.value, name: formElement.id, type: e.currentTarget.type, files: e.currentTarget.files });
          accepted = value => inputChangedHandler({ value: value, name: formElement.id });
        }

        let form = (
          <Inputs
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            invalid={invalid}
            shouldValidate={shouldValidate}
            touched={touched}
            changed={changed}
            accepted={accepted}
            removeHandel={index => removeHandel(index, formElement.id)}
            label={formElement.config.label}
            progress={state.progressPercentImage}
            dropDownData={dropDownData}
            checkSubmited={checkSubmited}
          />
        );

        return form;
      })}
    </form>
  );
};

export default FormInputDiscount;

import React, { useState, useEffect, useMemo } from "react";
import "./index.scss";
import { Button, ButtonToolbar } from "react-bootstrap";
import { post, put, get, patch } from "../../../../api";
import DetailsStringInput from "../../../../components/DetailsComponent/DetailsStringInput";
import DetailsThumbnail from "../../../../components/DetailsComponent/DetailsThumbnail";
import DetailsCheckBox from "../../../../components/DetailsComponent/DetailsCheckBox";
import DetailsArrayTextArea from "../../../../components/DetailsComponent/DetailsArrayTextArea";
import DetailsPrice from "../../../../components/DetailsComponent/DetailsPrice";
import DetailsInputSearch from "../../../../components/DetailsComponent/DetailsInputSearch";
import DetailsArrayThumbnail from "../../../../components/DetailsComponent/DetailsArrayThumbnail";
import toastify from "../../../../util/toastify";
const DiscountDetails = ({ information, handelback, sendNewValData }) => {
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(true);
  let CatSearch = [];
  let CatInfo = { value: information.category._id, title: information.category.titleFa };

  useEffect(() => {
    get.categoreis(setCategories, setLoading);
  }, []);
  for (const index in categories) CatSearch.push({ value: categories[index]._id, title: categories[index].titleFa });
  // ================================================= modalInputs ======================

  const sendNewVal = async data => {
    console.log({ data });
    let param;
    param = Object.assign({ data }, { _id: information._id });
    sendNewValData(param);
  };

  // =================================================End modalInputs ======================
  console.log({ information });
  const elements = (
    <React.Fragment>
      {/* {renderModalInputs} */}
      <div className="show-card-elements-details opacity-Fade-in-adn-slide-top ">
        <ButtonToolbar>
          <Button onClick={handelback} variant="outline-primary">
            بازگشت <span>{">>>"}</span>
          </Button>
        </ButtonToolbar>
        <DetailsThumbnail toastify={toastify} label={"عکس"} elementType={"inputFile"} imageType={"thumbnail"} fieldName={"thumbnail"} cover={information.thumbnail} sendNewVal={sendNewVal} />

        <DetailsArrayThumbnail toastify={toastify} label={"اسلایدس"} elementType={"inputFile"} imageType={"thumbnail"} fieldName={"slides"} cover={information.slides} sendNewVal={sendNewVal} />

        <div className="detailsRow-wrapper">
          <div>
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.title} fieldName={"title"} label={"عنوان"} />
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.subTitle} fieldName={"subTitle"} label={"عنوان فرعی"} />
            <DetailsPrice toastify={toastify} sendNewVal={sendNewVal} Info={information.realPrice} fieldName={"realPrice"} label={"قیمت اصلی"} />
            <DetailsPrice toastify={toastify} sendNewVal={sendNewVal} Info={information.newPrice} fieldName={"newPrice"} label={"قیمت جدید"} />
          </div>
          <div>
            <DetailsCheckBox toastify={toastify} sendNewVal={sendNewVal} Info={information.isActive} fieldName={"isActive"} questions={{ question1: "فعال", question2: "غیر فعال" }} label={"وضعیت"} />
            <DetailsArrayTextArea toastify={toastify} sendNewVal={sendNewVal} Info={information.features} fieldName={"features"} label={"ویژگی "} />
            <DetailsArrayTextArea toastify={toastify} sendNewVal={sendNewVal} Info={information.termsOfUse} fieldName={"termsOfUse"} label={"شرایط استفاده "} />
            <DetailsInputSearch toastify={toastify} sendNewVal={sendNewVal} Info={CatInfo} searchInfo={CatSearch} fieldName={"category"} label={"دسته بندی  "} />
            <DetailsArrayTextArea toastify={toastify} sendNewVal={sendNewVal} Info={information.comments} fieldName={"comments"} label={"  نظرات "} />
          </div>
        </div>
      </div>
    </React.Fragment>
  );

  return elements;
};

export default DiscountDetails;

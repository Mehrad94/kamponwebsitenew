import React, { useEffect, useState } from "react";
import { get, deletes, patch } from "../../../api";
import DiscountDetails from "./DiscountDetails";
import ShowCardInformation from "../../../components/ShowCardInformation";
import BoxLoading from "react-loadingg/lib/BoxLoading";
import card from "../../../util/consts/card";

const DiscountsScreen = () => {
  const [Discounts, setDiscounts] = useState([]);
  const [loading, setLoading] = useState(true);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [state, setstate] = useState();
  console.log(Discounts);
  let index = 0;

  useEffect(() => {
    _handelShowSections(acceptedIndex);
  }, [state]);

  useEffect(() => {
    apiDiscounts();
  }, []);

  const apiDiscounts = async () => {
    return await get.discounts(setDiscounts, setLoading);
  };

  const _handelShowSections = index => {
    setAcceptedIndex(index);
  };
  const _handelback = () => {
    setAcceptedIndex();
    apiDiscounts();
  };
  const refreshComponent = async () => {
    if (await apiDiscounts()) {
      setstate(index++);
    }
  };
  const sendNewValData = async param => {
    let Info = await patch.editDiscount(param);
    if (Info) refreshComponent();
  };

  const optionClick = async event => {
    switch (event.mission) {
      case "remove":
        if (await deletes.discount(event._id)) refreshComponent();
        break;
      case "block":
        let data = { fieldChange: "isActive", newValue: event.value.toString() };
        let param = Object.assign({ data }, { _id: event._id });
        sendNewValData(param);
        break;
      default:
        break;
    }
  };
  const showDataElement =
    Discounts.length > 0 ? (
      acceptedIndex >= 0 ? (
        <React.Fragment>
          <DiscountDetails information={Discounts[acceptedIndex]} handelback={_handelback} sendNewValData={sendNewValData} />
        </React.Fragment>
      ) : (
        <ShowCardInformation data={card.discount(Discounts)} options={{ remove: true, block: true }} onClick={_handelShowSections} optionClick={optionClick} submitedTitle={"مشاهده جزيیات"} />
      )
    ) : (
      ""
    );
  return (
    <React.Fragment>
      <div className="countainer-main">{loading ? <BoxLoading size={"large"} /> : <React.Fragment>{showDataElement}</React.Fragment>}</div>;
    </React.Fragment>
  );
};

export default DiscountsScreen;

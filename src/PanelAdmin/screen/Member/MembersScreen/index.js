import React, { useEffect, useState } from "react";
import { get } from "../../../api";

const MembersScreen = () => {
  const [data, setData] = useState([]);
  const loading = () => true;
  useEffect(() => {
    get.members(setData, loading);
  }, []);
  console.log({ data });

  return <div>member</div>;
};

export default MembersScreen;

import React, { useState, useEffect, useMemo } from "react";
import "./index.scss";
import { Button, ButtonToolbar } from "react-bootstrap";
import DetailsArrayInput from "../../../../components/DetailsComponent/DetailsArrayInput";
import DetailsThumbnail from "../../../../components/DetailsComponent/DetailsThumbnail";
import DetailsCheckBox from "../../../../components/DetailsComponent/DetailsCheckBox";
import SimpleExample from "../../../../components/SimpleExample";
import DetailsStringTextArea from "../../../../components/DetailsComponent/DetailsStringTextArea";
import DetailsStringInput from "../../../../components/DetailsComponent/DetailsStringInput";
import toastify from "../../../../util/toastify";
import { patch } from "../../../../api";
const OwnerDetails = ({ information, handelback, refreshComponent, sendNewValData }) => {
  // ================================================= modalInputs ======================

  // =================================================End modalInputs ======================
  console.log({ information });
  const owner = [
    {
      city: "rashty",
      title: information.title,
      address: information.district + " ، " + information.address,
      telephone: information.phone,
      mobile: information.phoneNumber,
      image: information.thumbnail,
      location: information.coordinate
    }
  ];
  const sendNewVal = async data => {
    let param;
    param = Object.assign({ data }, { _id: information._id });
    sendNewValData(param);
  };
  const elements = (
    <React.Fragment>
      <div className="show-card-elements-details opacity-Fade-in-adn-slide-top ">
        <ButtonToolbar>
          <Button onClick={handelback} variant="outline-primary">
            بازگشت <span>{">>>"}</span>
          </Button>
        </ButtonToolbar>
        <DetailsThumbnail toastify={toastify} sendNewVal={sendNewVal} label={"عکس"} elementType={"inputFile"} imageType={"thumbnail"} fieldName={"thumbnail"} cover={information.thumbnail} sendNewVal={sendNewVal} />

        <div className="detailsRow-wrapper">
          <div>
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.title} Id={information._id} fieldName={"title"} label={"عنوان"} />
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.subTitle} Id={information._id} fieldName={"title"} label={"عنوان فرعی"} />
            <DetailsCheckBox toastify={toastify} sendNewVal={sendNewVal} Info={information.isActive} Id={information._id} fieldName={"isActive"} questions={{ question1: "فعال", question2: "غیر فعال" }} label={"وضعیت"} />
          </div>
          <div>
            <DetailsArrayInput toastify={toastify} sendNewVal={sendNewVal} Info={information.phone} Id={information._id} fieldName={"phone"} label={"تلفن ثابت"} />
            <DetailsStringInput toastify={toastify} sendNewVal={sendNewVal} Info={information.phoneNumber} Id={information._id} fieldName={"phoneNumber"} label={"شماره موبایل"} />
            <DetailsStringTextArea toastify={toastify} sendNewVal={sendNewVal} Info={information.address} label={" آدرس"} />
          </div>
        </div>

        <SimpleExample data={owner} center={information.coordinate} />
      </div>
    </React.Fragment>
  );

  return elements;
};

export default OwnerDetails;

import React, { useEffect, useState } from "react";
import "./index.scss";
import { get, patch, deletes } from "../../../api";
import ShowCardInformation from "../../../components/ShowCardInformation";
import OwnerDetails from "./OwnerDetails";
import { BoxLoading } from "react-loadingg";
import "react-toastify/dist/ReactToastify.css";
import card from "../../../util/consts/card";
import Patch from "../../../api/Patch";

const OwnerScreen = () => {
  const [Owners, setOwner] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadingUploadImage, setLoadingUploadImage] = useState(false);
  const [acceptedIndex, setAcceptedIndex] = useState();
  const [state, setstate] = useState();
  let index = 0;
  useEffect(() => {
    _handelShowSections(acceptedIndex);
  }, [state]);

  useEffect(() => {
    apiOwners();
  }, []);

  const apiOwners = async () => {
    return await get.Owners(setOwner, setLoading);
  };

  const _handelShowSections = index => {
    setAcceptedIndex(index);
  };
  const _handelback = () => {
    setAcceptedIndex();
    apiOwners();
  };
  const refreshComponent = async () => {
    if (await apiOwners()) {
      setstate(index++);
    }
  };
  const sendNewValData = async param => {
    let Info = await patch.owner(param);
    if (Info) refreshComponent();
  };
  const optionClick = async event => {
    switch (event.mission) {
      case "remove":
        if (await deletes.owner(event._id)) refreshComponent();
        break;
      case "block":
        let data = { fieldChange: "isActive", newValue: event.value.toString() };
        let param = Object.assign({ data }, { _id: event._id });
        sendNewValData(param);
        break;
      default:
        break;
    }
  };

  const showDataElement =
    Owners.length > 0 ? (
      acceptedIndex >= 0 ? (
        <React.Fragment>
          <OwnerDetails information={Owners[acceptedIndex]} handelback={_handelback} sendNewValData={sendNewValData} />
        </React.Fragment>
      ) : (
        <ShowCardInformation data={card.owner(Owners)} options={{ block: true }} onClick={_handelShowSections} optionClick={optionClick} submitedTitle={"مشاهده جزيیات"} />
      )
    ) : (
      ""
    );
  return (
    <React.Fragment>
      <div className="countainer-main">{loading ? <BoxLoading size={"large"} /> : <React.Fragment>{showDataElement}</React.Fragment>}</div>;
    </React.Fragment>
  );
};

export default OwnerScreen;

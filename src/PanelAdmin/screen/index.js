import React from "react";
import "./index.scss";
import { Route } from "react-router-dom";
import SideMenu from "../components/SideMenu";
import Header from "../components/Header";
import PanelAdminMain from "../components/PanelAdminMain";

const PanelAdminScreens = () => {
  return (
    <div className="panelAdmin-container">
      {/* <SideMenu /> */}
      <div className="panelAdmin-content">
        {/* <Header /> */}
        <PanelAdminMain />
      </div>
    </div>
  );
};

export default PanelAdminScreens;

import React from "react";
import "./index.scss";
const LoadingDot1 = ({ width, height }) => {
  return (
    <div class="sk-chase" style={{ width, height }}>
      <div class="sk-chase-dot"></div>
      <div class="sk-chase-dot"></div>
      <div class="sk-chase-dot"></div>
      <div class="sk-chase-dot"></div>
      <div class="sk-chase-dot"></div>
      <div class="sk-chase-dot"></div>
    </div>
  );
};

export default LoadingDot1;

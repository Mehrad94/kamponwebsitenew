import React from "react";
import PanelString from "../../../value/PanelString";

const discount = data => {
  const cardFormat = [];
  for (let index in data) {
    let title = data[index].title ? data[index].title : "";
    let owner = data[index].owner ? data[index].owner : "";
    let ownerTitle = owner.ownerTitle ? owner.ownerTitle : "";
    let ownerDistrict = owner.district ? owner.district : "";
    let subTitle = data[index].subTitle ? data[index].subTitle : "";
    let boughtCount = data[index].boughtCount ? data[index].boughtCount : "";
    let realPrice = data[index].realPrice ? data[index].realPrice : "";
    let newPrice = data[index].newPrice ? data[index].newPrice : "";
    let viewCount = data[index].viewCount ? data[index].viewCount : "";
    let category = data[index].category ? data[index].category : "";
    let categoryTitleFa = category.titleFa ? category.titleFa : "";

    cardFormat.push({
      _id: data[index]._id,
      isActive: data[index].isActive,
      image: { value: data[index].thumbnail },
      body: [
        {
          right: [{ elementType: "text", value: title, style: { color: "black", fontSize: "1.3em", fontWeight: "bold" } }],
          left: [{ elementType: "text", value: categoryTitleFa, style: { color: PanelString.color.GREEN, fontSize: "1em", fontWeight: "800" } }]
        },
        {
          right: [{ elementType: "text", value: subTitle, title: subTitle, style: { color: PanelString.color.GRAY, fontSize: "1em", fontWeight: "500" } }],
          left: [{ elementType: "text", value: ownerTitle, style: { color: PanelString.color.GRAY, fontSize: "1em", fontWeight: "500" } }]
        },
        {
          right: [{ elementType: "district", value: ownerDistrict }],
          left: [
            { elementType: "icon", value: boughtCount, className: "icon-basket", direction: "ltr", style: { fontSize: "1.4em", fontWeight: "500" }, iconStyle: { fontSize: "1.4em" } },
            { elementType: "icon", value: viewCount, className: "icon-eye", direction: "ltr", style: { fontSize: "1.4em", fontWeight: "500", marginRight: "0.3rem" }, iconStyle: { fontSize: "1.4em" } }
          ]
        },
        {
          right: [{ elementType: "price", value: realPrice, style: { color: "red", fontSize: "1em", fontWeight: "500", textDecoration: "line-through" } }],
          left: [{ elementType: "price", value: newPrice, style: { color: "green", fontSize: "1.5em", fontWeight: "500" } }]
        }
      ]
    });
  }
  return cardFormat;
};

export default discount;

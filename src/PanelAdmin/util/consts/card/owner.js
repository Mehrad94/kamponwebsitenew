import React from "react";
import PanelString from "../../../value/PanelString";

const owner = data => {
  const cardFormat = [];
  for (let index in data) {
    cardFormat.push({
      _id: data[index]._id,
      isActive: data[index].isActive,
      image: { value: data[index].thumbnail },
      body: [
        {
          right: [{ elementType: "text", value: data[index].title, style: { color: "black", fontSize: "1.3em", fontWeight: "bold" } }],
          left: [{ elementType: "star", value: data[index].rating, direction: "ltr" }]
        },
        { right: [{ elementType: "text", value: data[index].subTitle, title: data[index].subTitle, style: { color: PanelString.color.GRAY, fontSize: "0.9em", fontWeight: "500" } }] },

        {
          right: [{ elementType: "district", value: data[index].district }],
          left: [{ elementType: "icon", value: data[index].phoneNumber, className: "icon-phone", direction: "ltr" }]
        }
      ]
    });
  }
  return cardFormat;
};

export default owner;

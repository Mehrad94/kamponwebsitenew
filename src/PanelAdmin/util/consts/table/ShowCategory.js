import React from "react";

const ShowCategory = Categories => {
  const thead = ["شماره", "عکس", "عنوان انگلیسی", "عنوان فارسی", "زیر مجموعه", "وزن", "تنظیمات"];
  let tbody = [];
  for (let index = 0; index < Categories.length; index++) {
    let subCategory = Categories[index].subCategories.length ? Categories[index].subCategories : "ندارد";

    tbody.push([Categories[index].image, Categories[index].titleEn, Categories[index].titleFa, subCategory, Categories[index].weight, { option: { edit: true, remove: true } }]);
  }
  return [thead, tbody];
};

export default ShowCategory;

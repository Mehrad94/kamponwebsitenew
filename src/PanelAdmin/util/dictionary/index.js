import React from "react";

const dictionary = text => {
  let translated;

  switch (text) {
    case "APPLICATION":
      translated = "اپلیکشن";
      break;
    case "CARD":
      translated = "کارت";
      break;
    case "DISCOUNT":
      translated = "تخفیف";
      break;
    case "club":
      translated = "باشگاه";
      break;
    default:
      break;
  }
  return translated;
};

export default dictionary;

const constArray = {
  type: {
    label: "نوع بلاگ",
    type: "button",

    text: [
      { title: "تصویری", value: "VIDEO" },
      { title: "صوتی", value: "VOICE" }
    ]
  },
  title: { label: "عنوان", type: "text" },
  description: { label: "توضیحات", type: "text" },
  upload: { label: "آپلود فایل", type: "file" }
};
const addSectionsArray = [];
for (const key in constArray) {
  addSectionsArray.push({
    name: key,
    value: constArray[key]
  });
}
export default addSectionsArray;

import addSectionsArray from "./addSectionsArray";
import addSliderArray from "./addSliderArray";
import editBlogArray from "./blog/editBlogArray";
import addCategoryArray from "./addCategoryArray";

const inputArray = { addSectionsArray, addSliderArray, editBlogArray, addCategoryArray };
export default inputArray;

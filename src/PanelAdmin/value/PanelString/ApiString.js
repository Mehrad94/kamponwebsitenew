const ADMIN = "/admin";
const UPLOAD = ADMIN + "/upload";
const CATEGORY = ADMIN + "/category";
const CATEGORY_SEARCH = ADMIN + "/category/s";
const SLIDER = ADMIN + "/slider";
const LOGIN = ADMIN + "/login";
const OWNER = ADMIN + "/owner";
const OWNER_SEARCH = ADMIN + "/owner/s";
const DISCOUNT = ADMIN + "/discount";
const DISCOUNT_SEARCH = ADMIN + "/discount/s";
const CLUB = ADMIN + "/club";
const CLUB_SEARCH = ADMIN + "/club/s";
const PROFILE = "/profile";
const BANNERS = ADMIN + "/banner";
// const BANNERS = ADMIN + "/baner";

const ApiString = {
  UPLOAD,
  CATEGORY,
  CATEGORY_SEARCH,
  SLIDER,
  LOGIN,
  OWNER,
  OWNER_SEARCH,
  DISCOUNT,
  DISCOUNT_SEARCH,
  CLUB,
  CLUB_SEARCH,
  PROFILE,
  BANNERS
};
export default ApiString;

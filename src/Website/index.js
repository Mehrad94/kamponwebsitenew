import React from "react";
import { Switch, Route } from "react-router-dom";
import HomeScreen from "./screen/HomeScreen";
import SubmitPayment from "./screen/SubmitPayment";

const WebSite = () => {
  return (
    <Switch>
      <Route path="/submitPayment/:number" component={SubmitPayment} />
      <Route path="/" exact component={HomeScreen} />
    </Switch>
  );
};

export default WebSite;

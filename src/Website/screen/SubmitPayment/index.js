import React from "react";
import { Link } from "@material-ui/core";
import "./index.scss";
const SubmitPayment = props => {
  console.log(props.match.params.number);
  const URL_NUMBER = props.match.params.number;
  let messege;
  let classSpan;
  if (URL_NUMBER === "1") {
    messege = "شد";
    classSpan = "green";
  } else {
    messege = " نشد !!";
    classSpan = "red";
  }
  return (
    <div className="centerAll height100vh">
      <div className="SubmitPayment-wrapper">
        <div>
          {" "}
          {"پرداخت شما با موفقیت انجام "}
          <span className={classSpan}>{messege}</span>
        </div>
        <div className="btns-container">
          <Link to="https://www.kampon.ir/cart" className="btns btns-primary">
            بازگشت به اپ
          </Link>
        </div>
      </div>
    </div>
  );
};

export default SubmitPayment;
